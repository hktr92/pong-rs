use amethyst::{
    core::transform::Transform,
    prelude::*,
    renderer::Camera,
};

use crate::pong::{
    ARENA_WIDTH,
    ARENA_HEIGHT,
};

pub fn init(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_translation_xyz(ARENA_WIDTH * 0.5, ARENA_HEIGHT * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(ARENA_WIDTH, ARENA_HEIGHT))
        .with(transform)
        .build();
}