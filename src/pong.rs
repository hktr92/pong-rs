use amethyst::{
    assets::Handle,
    core::timing::Time,
    prelude::*,
    renderer::SpriteSheet,
};

use crate::{
    init::{
        audio,
        camera,
        sprite_sheet,
    },
    entity::{
        ball,
        paddle,
        score_board,
    },
};

pub const ARENA_HEIGHT: f32 = 100.0;
pub const ARENA_WIDTH: f32 = 100.0;

#[derive(Default)]
pub struct Pong {
    ball_spawn_timer: Option<f32>,
    sprite_sheet_handler: Option<Handle<SpriteSheet>>,
}

impl SimpleState for Pong {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        self.ball_spawn_timer.replace(1.0);
        self.sprite_sheet_handler.replace(sprite_sheet::load(world));

        paddle::init(world, self.sprite_sheet_handler.clone().unwrap());
        score_board::init(world);
        camera::init(world);
        audio::init(world);
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        if let Some(mut timer) = self.ball_spawn_timer.take() {
            {
                let time = data.world.fetch::<Time>();
                timer -= time.delta_seconds();
            }

            if timer <= 0.0 {
                ball::init(data.world, self.sprite_sheet_handler.clone().unwrap());
            } else {
                self.ball_spawn_timer.replace(timer);
            }
        }

        Trans::None
    }
}