use amethyst::{
    assets::Handle,
    core::transform::Transform,
    ecs::{
        Component,
        DenseVecStorage,
    },
    prelude::*,
    renderer::{
        SpriteRender,
        SpriteSheet,
    },
};

use crate::pong::{
    ARENA_HEIGHT,
    ARENA_WIDTH,
};

pub const BALL_VELOCITY_X: f32 = 75.0;
pub const BALL_VELOCITY_Y: f32 = 50.0;
pub const BALL_RADIUS: f32 = 2.0;

pub struct Ball {
    pub velocity: [f32; 2],
    pub radius: f32,
}

impl Ball {
    pub fn default() -> Self {
        Ball {
            radius: BALL_RADIUS,
            velocity: [BALL_VELOCITY_X, BALL_VELOCITY_Y],
        }
    }
}

impl Component for Ball {
    type Storage = DenseVecStorage<Self>;
}

pub fn init(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    let mut local_transform = Transform::default();
    local_transform.set_translation_xyz(
        ARENA_WIDTH / 2.0,
        ARENA_HEIGHT / 2.0,
        0.0,
    );

    let sprite_render = SpriteRender::new(
        sprite_sheet_handle,
        1,
    );

    world
        .create_entity()
        .with(sprite_render)
        .with(Ball::default())
        .with(local_transform)
        .build();
}